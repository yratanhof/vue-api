import { routes as auth } from '../modules/auth'
import { routes as home } from '../pages/home/'
import { routes as users } from '../pages/users'

export default [
  ...auth,
  ...home,
  ...users,
  {
    path: '*',
    name: '404',
    component: () => import(/* webpackChunkName: "404" */ '@/pages/404')
  }
]
