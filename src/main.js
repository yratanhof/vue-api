import Vue from 'vue'
import App from './App'
import store from './store'
import router from './router'

import './assets/scss/app.scss'
import vuetify from './plugins/vuetify'

import DefaultLayout from './layouts/Default'
import LoginLayout from './layouts/Login'

Vue.component('default-layout', DefaultLayout)
Vue.component('login-layout', LoginLayout)

Vue.config.productionTip = false

Vue.prototype.$bus = new Vue()

window._Vue = new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
