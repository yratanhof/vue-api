export default [
  {
    path: '/login',
    name: 'login',
    meta: { layout: 'login' },
    component: () => import(/* webpackChunkName: "login" */ './pages/Login')
  }
]
