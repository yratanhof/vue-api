import { services as auth } from '@/modules/auth'
import { services as users } from '@/pages/users'

export default {
  auth,
  users
}
