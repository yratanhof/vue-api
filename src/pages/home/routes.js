export default [
  {
    path: '/',
    name: 'home',
    component: () => import(/* webpackChunkName: "home" */ './Home')
  },
  {
    path: '/teste',
    name: 'teste',
    component: () => import(/* webpackChunkName: "home" */ '@/pages/Teste')
  }
]
