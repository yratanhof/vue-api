export default [
  {
    path: '/users',
    name: 'users',
    component: () => import(/* webpackChunkName: "users" */ './List')
  }
]
