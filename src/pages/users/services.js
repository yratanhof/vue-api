export default {
  list: { method: 'get', url: 'users' },
  update: { method: 'put', url: 'users/:id' },
  findById: { method: 'get', url: 'users/:id' }
}
